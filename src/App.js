import React from 'react';

import '../node_modules/normalize.css';
import './Pages/_Common/css/main.scss';

import './Pages/_Common/css/fonts.css';
import { CONST_ROUTS } from './Constants/CONST_ROUTS';


function App() {
  return (
    <>
      <CONST_ROUTS />
    </>
  );
}

export default App;
