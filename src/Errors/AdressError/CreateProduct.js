import React from 'react';


class AdressError extends React.Component {
  render() {
    return (
      <>
        <h1>We dont have this page!</h1>
      </>
    );
  }
}

export default AdressError;
